<?php
//vsechny chyby na displej
error_reporting(E_ALL);
ini_set('display_errors', 1);
//pЕ™ipojenГ­ k DB
$db = mysqli_connect("wa.toad.cz", "savvavic", "webove aplikace", "savvavic");
// pokud je chyba
if (mysqli_connect_errno()) {
	echo "Failed to connect to DB: " . mysqli_connect_error();
	die();
}
//komunikace s formulГЎЕ™em
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = $_POST["email"];
    $password = $_POST["password"];
	$pswrepeat=$_POST["pswrepeat"];
	// testovani na zleho uzivatele a vymazani zbytecnych symbolu
	$testemail = test_input($_POST["email"]);
    $testpassword = test_input($_POST["password"]);
	$testpassword2 = test_input($_POST["pswrepeat"]);
	$err="";
	//verifikace
	if (empty($email) || empty($password) || empty($pswrepeat)){
		$err = "Complete all fields.";}
		
    //stejnГЎ hesla
    if ($password != $pswrepeat){
        $err = "Passwords do not match.";}

    //email validace
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$err = "Enter a valid email.";}

    //dГ©lka hesla
    if (strlen($password) < 6){
        $err = "Choose a password at least 6 characters long.";}
	//dotaz na DB, jestli email jiz tam existuje
	$sql = "SELECT * FROM SAVVAVIC WHERE EMAIL = '$email'";
	$res = mysqli_query($db, $sql);
	$num = mysqli_num_rows($res);
	if ($num > 0) {
		$err = "Email already exists.";
	}
    if ($err=="") {
		//solenГ­ hesel
        $encryptPassword = password_hash($_POST["password"], PASSWORD_DEFAULT);
	    //uloЕѕenГ­ dat uЕѕivatele do DB
        $stmt = $db->prepare("INSERT INTO SAVVAVIC(EMAIL, PASSWORD) VALUES(?, ?)");
	    //promД›nnГ© -> parametry
        $stmt->bind_param("ss", $email, $encryptPassword);
        $stmt->execute();
	    //mnoЕѕstvГ­ zpracovanГЅch Е™ГЎdkЕЇ
        $result = $stmt->affected_rows;
        $stmt->close();
        $db->close();
	    //pЕ™i ГєspД›ЕЎnГ©m uloЕѕenГ­ pЕ™esmД›rovГЎnГ­ do strГЎnky s potvrzenГ­m registrace
        if ($result > 0) {
		    header("location: ../view/regsuccess.html");}
        else {
            echo "Registration failed. Please, try again. ";}
	}
	// pokud je chyba tak se zobrazi
	else {
		echo $err;
	}
}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}