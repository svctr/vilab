<?php
//zacatek session
session_start();
//zobrazovani chyb
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<?php // neprihlaseny nema pristup
if (!isset($_SESSION['user'])) {
	echo "<a href='../mainpage.php'>Log in</a> to continue.";
}
else {
	$userid=$_SESSION['user']; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Welcome</title>
    <meta charset="UTF-8">
    <link href="../styles/welcomestyles.css" rel="stylesheet" type="text/css"/>
  </head>
  <body>
    <div id="main">
      <div id="name1">
        <h1><a href="welcome.php">ViLab</a></h1>
      </div>
      <div id="name2">
          <h3>Your Personal Guide in the World of Science</h3>
      </div>
        <div id="nav">
           <a href="manual.rtf">How To</a>
           <a href="dokumentace.rtf">Documentation</a>
        </div>
        <form><button type="submit" class="browsing" formaction="browsing2.php">Start Browsing!</button></form>
		  <h2>Welcome, <?php echo $userid;?>!!!</h2>
		  <form><button type="submit" class="logout" formaction="logout.php">Log Out</button></form>
		</div>
	</body>
</html>
<?php } ?>