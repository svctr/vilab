<?php
session_start();
// pripojeni
$con=mysqli_connect("wa.toad.cz", "savvavic", "webove aplikace", "savvavic");
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $email = htmlspecialchars($_POST["email"]);
    $password = htmlspecialchars($_POST["password"]);
  $sql="SELECT EMAIL, PASSWORD FROM SAVVAVIC WHERE EMAIL = '$email'";
  // SQL dotaz
  $result=mysqli_query($con,$sql);
  // asociativni pole
  $row=mysqli_fetch_array($result,MYSQLI_ASSOC);
  // verifikace hesla
  if ($row && password_verify($_POST['password'], $row['PASSWORD'])) {
    //pokud v poradku tak prideleni session
    $_SESSION['user'] = $email;
    header("location: welcome.php");}
  else {
    echo "<span>Incorrect email or password. Click back and try again.</span>";}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>ViLab: Broadcasting the Knowledge</title>
    <meta charset="UTF-8">
    <link href="../styles/styles.css" rel="stylesheet" type="text/css"/>
    <script src="../scripts/scripts.js"></script>
  </head>
  <body>
    <div id="main">
      <div id="name1">
        <h1><a href="#">ViLab</a></h1>
      </div>
      <div id="name2">
          <h3>Your Personal Guide in the World of Science</h3>
      </div>
      <div id="nav">
          <a href="docs/manual.rtf">How To</a>
          <a href="docs/dokumentace.rtf">Documentation</a>
      </div>
      <div class="form" id="myForm">
        <form name="login" action="" method="post" class="form-container">
          <label for="email"><b>Email</b></label>
          <input type="text" id="email" placeholder="Enter Email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
          <label for="password"><b>Password</b></label>
          <input type="password" id="password" placeholder="Enter Password" name="password" pattern=".{6,}" required>
          <button type="submit" class="btn" onsubmit="return validate();">Log In</button>
        </form>
      </div>
      <form>
        <button type="submit" class="joinbutton" formaction="registrationpage.php">Join Us</button>
      </form>
      <form>
        <button type="submit" class="browsing" formaction="browsing.php">Start Browsing!</button>
      </form>
    </div>
  </body>
</html>