<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);?>
<?php
if (!isset($_SESSION['user'])) {
	echo "<a href='mainpage.php'>Log in</a> to continue.";
}
else {?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Search</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../styles/browsingstyles.css" type="text/css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="browsingscripts.js"></script>
	</head>
	<body>
		<form><button type="submit" id="favs" formaction="favs.php"><i class="fa fa-star"></i> My Favorite Items</button></form><br>

		<h1><a href="browsing2.php">Back to Start</a></h1><br>

		<form action="browsing2.php" id="search" name="search" method="get">

		<label for="name">Name: </label>
		<input id="name" type="text" name="name" placeholder="Resource Name">

		<label for="cat">Category: </label>
		<input id="cat" type="text" name="cat" placeholder="Biology / Chemistry / Physics...">

		<label for="lang">Language: </label>
		<input id="lang" type="text" name="lang" placeholder="English / German / French...">

		<label for="year">Year: </label>
		<input id="year" type="text" name="year" placeholder="Publication date"><br>

		<button type="submit" id="searchbutton" name="search">Search</button></form><br>

		<form><button type="submit" class="home" formaction="welcome.php">Home</button></form>

		<form><button type="submit" class="logout" formaction="logout.php">Log Out</button></form>
		<?php
        //pokud neni zaslan zadny formular s GETem
	    if (!isset($_GET['name'], $_GET['lang'], $_GET['year'], $_GET['cat'])) {
			// zajištení proměnné s číslem stránky
			if (isset($_GET['pageno'])) {
				$pageno = $_GET['pageno'];
			}
			else {
				$pageno = 1;
			}
			$records_per_page = 1;
			$offset = ($pageno-1)*$records_per_page;
			//připojení do DB
			$conn=mysqli_connect("wa.toad.cz","savvavic","webove aplikace","savvavic");
			//pokud se nezadařilo
			if (mysqli_connect_errno()){
				echo "Failed to connect to DB: " . mysqli_connect_error();
				die();
			}
			//spočítat množství položek
			$total_pages_sql = "SELECT COUNT(*) FROM RESOURCES";
			//dotaz na DB
			$result = mysqli_query($conn,$total_pages_sql);
			//množství položek
			$total_rows = mysqli_fetch_array($result)[0];
			//množství stránek; ceil = zaokrouhlení do největšího čísla
			$total_pages = ceil($total_rows / $records_per_page);
			//SQL příkaz pro paginaci/strankování
			$sql = "SELECT * FROM RESOURCES LIMIT $offset, $records_per_page";
			$res_data = mysqli_query($conn,$sql);
			//vypis radku
			while ($row = mysqli_fetch_array($res_data)) {
					echo "<h2>Category: <mark>".$row['CATEGORY']."</mark> Name: <mark>".$row['NAME']."</mark> Language: <mark>".$row['LANGUAGE']."</mark> Year: <mark>".$row['YEAR']."</mark><br><br>Link: <a id='read1' href='".$row['LINK']."'>Read</a> <a id='read2' href='".$row['LINK']."' download>Download</a><br><br><img src='".$row['PIC']."' alt='no pic:('><br></h2><form method='post'><button id='fav' type='submit'><i class='fa fa-heart'></i> Add to Favorites</button></form><br>";
					//pri stisknuti tlacitka 'add to favorites'
				if ($_SERVER['REQUEST_METHOD'] == 'POST') {
					$email = $_SESSION['user'];
					$name = $row['NAME'];
					$sql = "INSERT INTO FAVORITES(EMAIL, NAME) VALUES ('$email', '$name')";
					$res = mysqli_query($conn, $sql);
					echo "<h2>Added</h2><br>";
					//obrana proti zasilani nekolikrat
			?><script>
				window.onload = function() {
					history.replaceState("", "", "#");
				}
			</script><?php
				}
			}
			mysqli_close($conn);			
		}
		//pokud je zaslan formular s GETem
        else {
			$cat=htmlspecialchars($_GET['cat']);
		    $lang=htmlspecialchars($_GET['lang']);
		    $name=htmlspecialchars($_GET['name']);
		    $year=htmlspecialchars($_GET['year']);
			if (isset($_GET['pageno'])) {
                $pageno = $_GET['pageno'];
            } else {
                $pageno = 1;
            }
		    $records_per_page = 1;
            $offset = ($pageno-1)*$records_per_page;
		
		    //připojení do DB
            $conn=mysqli_connect("wa.toad.cz","savvavic","webove aplikace","savvavic");
		    //pokud se nezadařilo
            if (mysqli_connect_errno()){
                echo "Failed to connect to DB: " . mysqli_connect_error();
                die();
             }
		    //spočítat množství položek
		     $total_pages_sql = "SELECT COUNT(*) FROM RESOURCES WHERE CATEGORY = '$cat' OR NAME = '$name' OR LANGUAGE = '$lang' OR YEAR = '$year'";
		    //dotaz na DB
             $result = mysqli_query($conn,$total_pages_sql);
		     //množství položek
             $total_rows = mysqli_fetch_array($result)[0];
		     //množství stránek; ceil = zaokrouhlení do největšího čísla
             $total_pages = ceil($total_rows / $records_per_page);
		     //SQL příkaz pro paginaci/strankování
             $sql = "SELECT * FROM RESOURCES WHERE CATEGORY = '$cat' OR NAME = '$name' OR LANGUAGE = '$lang' OR YEAR = '$year' LIMIT $offset, $records_per_page";
             $res_data = mysqli_query($conn,$sql);
			 $a = mysqli_num_rows($res_data);
			 //pokud je match tak vypis
			 if ($a) {
				 //vypis radku
				 while($row = mysqli_fetch_array($res_data)){
					 echo "<h2>Category: <mark>".$row['CATEGORY']."</mark> Name: <mark>".$row['NAME']."</mark> Language: <mark>".$row['LANGUAGE']."</mark> Year: <mark>".$row['YEAR']."</mark><br><br>Link: <a id='read1' href='".$row['LINK']."'>Read</a> <a id='read2' href='".$row['LINK']."' download>Download</a><br><br><img src='".$row['PIC']."' alt='no pic:('></h2><br><form method='post'><button id='fav' type='submit'><i class='fa fa-heart'></i> Add to Favorites</button></form><br>";
					 if ($_SERVER['REQUEST_METHOD'] == 'POST') {
					   $email = $_SESSION['user'];
					   $name = $row['NAME'];
					   $sql = "INSERT INTO FAVORITES(EMAIL, NAME) VALUES ('$email', '$name')";
				       $res = mysqli_query($conn, $sql);
					   echo "<h2>Added</h2><br>";
			?><script>
    			window.onload = function() {
        			history.replaceState("", "", "#");
    			}
			</script><?php
			 		}
			 	}
			}
			 else {
				 echo "<h2>No results :(</h2><br>";
			 }
			  mysqli_close($conn);
		}
		?>
		<ul class="pagination">
			<li><a href="?pageno=1">First</a></li>
			<li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
				<a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev</a>
			</li>
			<li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
				<a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next</a>
			</li>
			<li><a href="?pageno=<?php echo $total_pages; ?>">Last</a></li>
		</ul>
	</body>
</html>
<?php } ?>